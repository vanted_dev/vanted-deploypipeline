#!/bin/sh

#figure if java runtime is x86 or am64
#assign ram to VM accordingly
# 33% of RAM but not more than 1GB on x86
# 33% of RAM but not more than 8GB on am64
echo "setting startup parameters"
# var=$(command) executes the the command which can be stored in the variable
# var
vailmem=$(free |awk '/'"Mem:"'/ {print $2}')
# var=$((formula)) evaluates the expression
# same as var=$(echo formula | bc) which uses bc to evaluate the formula
vailmem=$((vailmem/1024))
usemem=$((vailmem/3))

echo "available memory: $vailmem"

isarch=$(java -version 2>&1 |grep -c -i "64-bit")
if [ 0 -eq $isarch ] ; then
	arch=x86
	echo "32 bit java installed"
else
	arch=x86_64
	echo "64 bit java installed"
fi

if [ $usemem -gt 1000 ] ; then

	case $arch in
	'x86')
		usemem=1024
		;;
	*)
		if [ $usemem -gt 8000 ] ; then
			usemem=8000
		fi
		;;
	esac

fi
echo "memory for vanted: $usemem"
if [ -e $HOME/.vanted/startparams ] ; then 
	java -Dfile.encoding=UTF-8 `cat $HOME/.vanted/startparams` -jar $INSTALL_PATH/vanted-boot.jar
else
	java -Dfile.encoding=UTF-8 -Xmx${usemem}m -jar $INSTALL_PATH/vanted-boot.jar
fi
