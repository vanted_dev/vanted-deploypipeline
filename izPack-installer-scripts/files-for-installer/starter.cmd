@echo off
echo Checking System
set PHYSMEM=
for /f "tokens=2 delims==" %%A in ('wmic os get totalvisiblememorysize /value') do (set PHYSMEM=%%A)
set /a PHYSMEM = %PHYSMEM%/1024
echo physical memory %PHYSMEM%
set /a VANTEDMEM=%PHYSMEM%/3



REM check java runtime for 32 / 64bit
java -version 2>%TEMP%\java-version.txt
for /f %%A in ('findstr /n -i "64-bit" %TEMP%\java-version.txt^|find /c ":"') do set IS64BIT=%%A
del "%TEMP%\java-version.txt

if %IS64BIT%==0 (
	set ARCH=x86
) else (
	set ARCH=x64
)
echo Java RT architecture is %ARCH%


REM adjust memory usage for 32 and 64 bit systems
REM on 64 bit don't ever use more than 8GiBi
if %VANTEDMEM% gtr 1000 (
	if "%ARCH%"=="x86" (
		set VANTEDMEM=1024
	) else (
		if %VANTEDMEM% gtr 8000 (
			set VANTEDMEM=8000
		)
	)
) 
echo Vanted memory %VANTEDMEM%
java -Dfile.encoding=UTF-8 -Xmx%VANTEDMEM%m -jar vanted-boot.jar
