#!/bin/sh

#figure if x86 or am64
#assign ram to VM accordingly
# 50% of RAM but not more than 1GB on x86
# 50% of RAM but not more than 8GB on am64

# var=$(command) executes the the command which can be stored in the variable
# var
#vailmem=$(free |awk '/'"Mem:"'/ {print $2}')
# var=$((formula)) evaluates the expression
# same as var=$(echo formula | bc) which uses bc to evaluate the formula
#vailmem=$((vailmem/1024/1024))

FREE_BLOCKS=$(vm_stat | grep free | awk '{ print $3 }' | sed 's/\.//')
SPECULATIVE_BLOCKS=$(vm_stat | grep speculative | awk '{ print $3 }' | sed 's/\.//')
FREEMEM=$((($FREE_BLOCKS+SPECULATIVE_BLOCKS)*4096/1024/1024/1024))

TMP_AVAILMEM=$(sysctl -a | grep hw.memsize: | awk '{print $2}')
AVAILMEM=$((($TMP_AVAILMEM)*1/1024/1024/1024))

vailmem=$(( ($FREEMEM/2>$AVAILMEM/3?$FREEMEM/2:$AVAILMEM/3) ))

DIR=$(cd $(dirname $0);cd ..;pwd)

echo "available memory: $vailmem"
usemem=$(( ($FREEMEM/2>$AVAILMEM/3?$FREEMEM/2:$AVAILMEM/3) ))
echo "theoretically used memory for vanted: $usemem"

if [ $usemem -eq 0 ] ; then
	usemem=1
fi

if [ -e $HOME/.vanted/startparams ] ; then
	usemem=`cat $HOME/.vanted/startparams`
	echo "found config: using $usememM now" 
fi

/Library/Internet\ Plug-Ins/JavaAppletPlugin.plugin/Contents/Home/bin/java -Xmx${usemem}g -Xdock:name="VANTED" -Xdock:icon="${DIR}/Resources/vantedicon.icns" -jar ${DIR}/Java/vanted-boot.jar "$@"
