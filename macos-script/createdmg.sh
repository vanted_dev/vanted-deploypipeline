#!/bin/sh
find . -exec touch {} \;
mkdir target/MacOS/tempmount $2>/dev/null
rm target/MacOS/vanted.dmg
dd if=/dev/zero of=target/MacOS/vanted.dmg bs=1M count=60
mkfs.hfsplus -v Vanted target/MacOS/vanted.dmg
sudo mount -t hfsplus -o loop target/MacOS/vanted.dmg target/MacOS/tempmount
sudo cp -ar pre-build/MacOS/* target/MacOS/tempmount/
sudo cp -ar pre-build/MacOS/.DS_Store target/MacOS/tempmount/
sudo cp -ar pre-build/MacOS/.background target/MacOS/tempmount/

sudo umount -f target/MacOS/tempmount
rm -rf target/MacOS/tempmount
