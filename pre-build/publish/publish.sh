#!/bin/sh
BASEDIR=$1
PUBLISHPATH=$2
VERSION="2.6.4"
SERVER="web.kim.uni-konstanz.de"
USERNAME="dgarkov"
UPDATEWEBSITE=$6

# Get the updatemessage from the file in the root directory
# and put it in the vanted-update file

UPDATEMSG="$(grep '^[^#;]' ../../UPDATEMESSAGE)"
sed "s/%VERSION%/$VERSION/g;" update-file-template > vanted-update
echo "message:{{" >> vanted-update
echo $UPDATEMSG >> vanted-update
echo "}}\n//\n" >> vanted-update

# create new download HTML page with correct links to new version

sed "s/%VERSION%/$VERSION/g;" doc2_downloadPage_template.html > doc2.html

# create new md5-sum file, with all md5 sums from JAR files
echo creating md5sum file
find ../../target/Webstart/ -name *.jar -exec md5sum -b {} \; > vanted-files-md5 2>/dev/null

#creates new vanted version x.x.x subfolder and creates all parent subfolders, if missing
echo username $USERNAME
echo server $SERVER
echo publishpath $PUBLISHPATH
echo updatewebsite $UPDATEWEBSITE
echo version $VERSION

URL=$USERNAME@$SERVER

echo creating remote public directory on server : $URL
ssh $URL "mkdir -p $PUBLISHPATH/$VERSION 2>/dev/null"
echo copying all files
scp -r $BASEDIR/target/* $URL:$PUBLISHPATH/$VERSION

echo creating link to current folder

ssh $URL "rm $PUBLISHPATH/current" 2>/dev/null
ssh $URL "ln -s $PUBLISHPATH/$VERSION $PUBLISHPATH/current"
# setup update

# does final copying of necessary update files, to be found and used by VANTED

echo setting up update folder
ssh $URL "mkdir $PUBLISHPATH/updates 2>/dev/null"
scp $BASEDIR/target/Webstart/vanted-core.jar $URL:$PUBLISHPATH/updates/
scp vanted-update $URL:$PUBLISHPATH/updates/
scp vanted-files-md5 $URL:$PUBLISHPATH/updates/
ssh $URL "rm $PUBLISHPATH/updates/libs"
ssh $URL "ln -s $PUBLISHPATH/$VERSION/Webstart/libs/ $PUBLISHPATH/updates/libs"
scp ../../sources/vanted/CHANGELOG $URL:$PUBLISHPATH/updates/ 2>/dev/null
#copy download html file to vanted website
if [ $UPDATEWEBSITE == "yes" ]
then
	echo "updating website"
	scp doc2.html $URL:/html/vanted/
fi
