#!/bin/sh

LOCATION=/var/www/html/vanted-prerelease-test
echo "publishing develop version to pre-release location:"
echo $LOCATION

ant -Dwebserver.publish.dir=$LOCATION -Dpublish.website="no" publish
