#/bin/sh

# This script will check, if the necessary repositories for deployment are present.
# If they are missing it will clone the repositories.
# If they are present it will perform a pull on the existing ones
#
# This script will consider a file in the deployment root directory called 'BRANCH'
# Put the branch name in this file to specify the git branch to be cloned/pulled


echo 'looking for branch file'

if [ -f ../BRANCH ]; then
	BRANCH=$(cat ../BRANCH)
else
	BRANCH=master
fi

if [ -f ../GITREPOSITORY ]; then
	REPO=$(cat ../GITREPOSITORY)
else
	REPO=origin
fi

#URL=ssh://git@bitbucket.org/vanted-dev
URL=$1

echo 'checking for git repos'

if [ ! -d vanted ]; then
	echo 'vanted project not found > cloning from $URL'
	git clone $URL/vanted.git
fi

echo "vanted: checking out $BRANCH branch and pulling latest changes"
cd vanted
git reset --hard
git fetch $REPO
git checkout $BRANCH
git pull $REPO
cd ..

if [ -d vanted-lib-repository ]; then
	echo 'vanted-lib-repository: pulling latest changes'
	cd vanted-lib-repository
	git reset --hard
	git pull $REPO
	cd ..
else
	echo 'cloning vanted-lib-repository'
	git clone $URL/vanted-lib-repository.git
fi

if [ -d vanted-bootstrap ]; then
	echo 'vanted-bootstrap: pulling latest changes'
	cd vanted-bootstrap
	git reset --hard
	git pull $REPO	
	cd ..
else
	echo 'cloning vanted-bootstrap'
	git clone $URL/vanted-bootstrap.git
fi
	
