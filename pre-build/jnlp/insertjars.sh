#!/bin/sh

# this script collects all VANTED libraries jars and adds them to the
# JNLP files
VERSION=$1
TARGETDIR=$2
PATH_TO_LIB="../vanted-package/core-libs/"
HOME=`pwd`
OUTPUT=" "

cd $PATH_TO_LIB
for jarfile in *.jar
do
	OUTPUT="$OUTPUT <jar href=\\\"libs\/$jarfile\\\" main=\\\"false\"\/> \n"
done
# echo "$OUTPUT"
cd $HOME
sed "s/%JARS%/$OUTPUT/g; s/%VERSION%/$VERSION/g" starttemplate.jnlp > "$TARGETDIR/start.jnlp"
sed "s/%JARS%/$OUTPUT/g; s/%VERSION%/$VERSION/g" start64template.jnlp > "$TARGETDIR/start64.jnlp"
