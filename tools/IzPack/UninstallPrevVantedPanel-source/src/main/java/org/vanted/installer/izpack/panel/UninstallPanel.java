/**
 * 
 */
package org.vanted.installer.izpack.panel;

import java.awt.LayoutManager2;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import com.izforge.izpack.gui.IzPanelLayout;
import com.izforge.izpack.gui.LabelFactory;
import com.izforge.izpack.installer.InstallData;
import com.izforge.izpack.installer.InstallerFrame;
import com.izforge.izpack.installer.IzPanel;

/**
 * @author matthiak
 */
public class UninstallPanel extends IzPanel {
	
	boolean found = false;
	String vantedinstallname = null;
	
	public UninstallPanel(InstallerFrame paramInstallerFrame, InstallData paramInstallData)
	{
		this(paramInstallerFrame, paramInstallData, new IzPanelLayout());
	}
	
	public UninstallPanel(InstallerFrame paramInstallerFrame, InstallData paramInstallData, LayoutManager2 paramLayoutManager2)
	{
		super(paramInstallerFrame, paramInstallData, paramLayoutManager2);
		if( ! System.getProperty("os.name").toLowerCase().contains("windows"))
			return;

		try {
			List<String> readStringSubKeys = WinRegistry.readStringSubKeys(WinRegistry.HKEY_LOCAL_MACHINE,
					"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall");
			
			for (String s : readStringSubKeys) {
				if (s.toLowerCase().contains("vanted")) {
					found = true;
					vantedinstallname = s;
				}
			}
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
		
		JLabel localJLabel1 = LabelFactory.create("<html><center>Found previous version of VANTED: <br/>" + vantedinstallname + 
							"<br/><br/><br/><strong>It is recommended to uninstall a previous version.<br/>", paramInstallerFrame.icons.getImageIcon("host"), 10);
		
		add(localJLabel1, "nextLine");
		add(IzPanelLayout.createParagraphGap());
		add(IzPanelLayout.createParagraphGap());
		
		JButton buttonUninstall = new JButton("Uninstall " + vantedinstallname);
		buttonUninstall.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					String readStringSubKeys2 = WinRegistry.readString(WinRegistry.HKEY_LOCAL_MACHINE,
							"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\" + vantedinstallname, "UninstallString");
					
					Process exec = Runtime.getRuntime().exec(readStringSubKeys2);
					exec.waitFor();
					
					WinRegistry.deleteKey(WinRegistry.HKEY_LOCAL_MACHINE,
							"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Uninstall\\" + vantedinstallname);
				} catch (IllegalArgumentException e1) {
					e1.printStackTrace();
				} catch (IllegalAccessException e1) {
					e1.printStackTrace();
				} catch (InvocationTargetException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
				
			}
		});
		add(buttonUninstall, "nextLine");
		
		getLayoutHelper().completeLayout();
	}
	
	public boolean isValidated()
	{
		return true;
	}
	
	@Override
	public void panelActivate() {
		if (!found) {
			//JOptionPane.showMessageDialog(null, "no previous vanted found");
			parent.skipPanel();
		} else {
			//iJOptionPane.showMessageDialog(null, "previous vanted found");
		}
		
	}
	
}
